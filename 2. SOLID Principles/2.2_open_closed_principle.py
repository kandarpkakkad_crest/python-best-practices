"""
Open/Closed Principle.

This principle states that “software entities (classes, modules, functions, etc.) should be open
for extension, but closed for modification” which means you should be able to extend a class
behavior, without modifying it.

Suppose developer A needs to release an update for a library or framework and developer B wants
some modification or add some feature on that then developer B is allowed to extend the existing
class created by developer A but developer B is not supposed to modify the class directly. Using
this principle separates the existing code from the modified code so it provides better stability,
maintainability and minimizes changes as in your code.
"""


class Animal:
    """Define an animal."""

    def __init__(self, name: str):
        r"""Initialize an animal.

        Args: \
            `name (str)`: Name of the animal.
        """
        self.name = name

    def get_name(self) -> str:
        r"""Get name.

        Returns: \
            `str`: Name of the animal.
        """
        return self.name


animals = [Animal("lion"), Animal("mouse")]


def animal_sound(animals: list):
    r"""Sound of animal.

    Args: \
        `animals (list)`: Animals present.
    """
    for animal in animals:
        if animal.name == "lion":
            print("roar")

        elif animal.name == "mouse":
            print("squeak")


animal_sound(animals)

"""
The function animal_sound does not conform to the open-closed principle because
it cannot be closed against new kinds of animals.  If we add a new animal,
Snake, We have to modify the animal_sound function.  You see, for every new
animal, a new logic is added to the animal_sound function.  This is quite a
simple example. When your application grows and becomes complex, you will see
that the if statement would be repeated over and over again in the animal_sound
function each time a new animal is added, all over the application.
"""

animals = [Animal("lion"), Animal("mouse"), Animal("snake")]


def animal_sound(animals: list):
    r"""Sound of animal.

    Args: \
        `animals (list)`: Animals present.
    """
    for animal in animals:
        if animal.name == "lion":
            print("roar")
        elif animal.name == "mouse":
            print("squeak")
        elif animal.name == "snake":
            print("hiss")


animal_sound(animals)


"""
How do we make it (the animal_sound) conform to OCP?
"""


class AnimalOne:
    """Define an animal."""

    def __init__(self, name: str):
        r"""Initialize an animal.

        Args: \
            `name (str)`: Name of the animal.
        """
        self.name = name

    def get_name(self) -> str:
        r"""Get name.

        Returns: \
            `str`: Name of the animal.
        """
        self.name

    def make_sound(self):
        """Make sound."""
        pass


class Lion(AnimalOne):
    """Lion class."""

    def make_sound(self):
        """Make sound."""
        return "roar"


class Mouse(AnimalOne):
    """Mouse class."""

    def make_sound(self):
        """Make sound."""
        return "squeak"


class Snake(AnimalOne):
    """Snake class."""

    def make_sound(self):
        """Make sound."""
        return "hiss"


def animal_sound(animals: list):
    r"""Sound of animal.

    Args: \
        `animals (list)`: Animals present.
    """
    for animal in animals:
        print(animal.make_sound())


animal_sound(animals)

"""
Animal now has a virtual method make_sound. We have each animal extend the
Animal class and implement the virtual make_sound method.
Every animal adds its own implementation on how it makes a sound in the
make_sound.  The animal_sound iterates through the array of animal and just
calls its make_sound method.
Now, if we add a new animal, animal_sound doesn’t need to change.  All we need
to do is add the new animal to the animal array.
animal_sound now conforms to the OCP principle.
"""
