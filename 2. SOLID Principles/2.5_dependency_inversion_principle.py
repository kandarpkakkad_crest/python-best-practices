"""
Dependency Inversion Principle.

Two key points are here to keep in mind about this principle:

    * High-level modules/classes should not depend on low-level modules/classes. Both should
    depend upon abstractions.

    * Abstractions should not depend upon details. Details should depend upon abstractions.

The above lines simply state that if a high module or class will be dependent more on low-level
modules or class then your code would have tight coupling and if you will try to make a change in
one class it can break another class which is risky at the production level. So always try to make
classes loosely coupled as much as you can and you can achieve this through abstraction. The main
motive of this principle is decoupling the dependencies so if class A changes the class B doesn’t
need to care or know about the changes.

You can consider the real-life example of a TV remote battery. Your remote needs a battery but it’s
not dependent on the battery brand. You can use any XYZ brand that you want and it will work. So we
can say that the TV remote is loosely coupled with the brand name. Dependency Inversion makes your
code more reusable.
"""

from enum import Enum
from abc import abstractmethod


class Teams(Enum):
    """All Teams."""

    BLUE_TEAM = 1
    RED_TEAM = 2
    GREEN_TEAM = 3


class Student:
    """Define a student."""

    def __init__(self, name):
        r"""Initialize student.

        Args: \
            `name (str)`: Name of the student.
        """
        self.name = name


class TeamMemberships:
    """Team Membership."""

    def __init__(self):
        """Initialize team membership."""
        self.team_memberships = []

    def add_team_memberships(self, student, team):
        r"""Add team membership.

        Args: \
            `student (Student)`: Student.
            `team (Teams)`: Team
        """
        self.team_memberships.append((student, team))


class Analysis:
    """Analysis class."""

    def __init__(self, team_student_memberships):
        r"""Initialize analysis.

        Args: \
            `team_student_memberships (TeamMemberships)`: Team membership.
        """
        memberships = team_student_memberships.team_memberships
        for members in memberships:
            if members[1] == Teams.RED_TEAM:
                print(f"{members[0].name} is in RED team")


student1 = Student("Ravi")
student2 = Student("Archie")
student3 = Student("James")

team_memberships = TeamMemberships()
team_memberships.add_team_memberships(student1, Teams.BLUE_TEAM)
team_memberships.add_team_memberships(student2, Teams.RED_TEAM)
team_memberships.add_team_memberships(student3, Teams.GREEN_TEAM)

Analysis(team_memberships)

"""
To comply to Dependency Inversion Principle, we need to ensure that high level class Anslysis
should not depend on concrete implementation of low level class TeamMemberships. Instead it should
depend on some abstraction.
So, we create an interface TeamMembershipLookup that contains an abstract method find_all_students
of_team which is passed to any class that inherits from this interface. We make our TeamMembership
class to inherit from this interface and hence now TeamMembership class needs to provide an
implementation of the find_all_students_of_team function. This function then yields the results to
any other calling entity. We moved the processing that was done in high-level Analysis class to
TeamMemberships class through the interface TeamMembershipLookup.
So, by doing this we have removed dependency of high level class Analysis from low level class
TeamMemberships and transferred this dependency to interface TeamMembershipLookup. Now the
high-level class doesn’t depend on implementation details of low level class. Any changes to the
implementation details of low level class doesn’t impact the high-level class.
"""


class Teams(Enum):
    """All Teams."""

    BLUE_TEAM = 1
    RED_TEAM = 2
    GREEN_TEAM = 3


class TeamMembershipLookup:
    """Team membership lookup."""

    @abstractmethod
    def find_all_students_of_team(self, team):
        r"""Find students.

        Args: \
            `team (Teams)`: Team
        """
        pass


class Student:
    """Define student."""

    def __init__(self, name):
        r"""Initilialize student.

        Args: \
            `name (str)`: Name of the student.
        """
        self.name = name


class TeamMemberships(TeamMembershipLookup):
    """Team Membership."""

    def __init__(self):
        """Initialize team membership."""
        self.team_memberships = []

    def add_team_memberships(self, student, team):
        r"""Add team membership.

        Args: \
            `student (Student)`: Student.
            `team (Teams)`: Team
        """
        self.team_memberships.append((student, team))

    def find_all_students_of_team(self, team):
        r"""Find students.

        Args: \
            `team (Teams)`: Team

        Yields: \
            `str`: Name of the student.
        """
        for members in self.team_memberships:
            if members[1] == team:
                yield members[0].name


class Analysis:
    """Analysis class."""

    def __init__(self, team_membership_lookup):
        r"""Initialize analysis.

        Args: \
            `team_membership_lookup (TeamMembershipLookup)`: Team membership.
        """
        for student in team_membership_lookup.find_all_students_of_team(Teams.RED_TEAM):
            print(f"{student} is in RED team.")


student1 = Student("Ravi")
student2 = Student("Archie")
student3 = Student("James")

team_memberships = TeamMemberships()
team_memberships.add_team_memberships(student1, Teams.BLUE_TEAM)
team_memberships.add_team_memberships(student2, Teams.RED_TEAM)
team_memberships.add_team_memberships(student3, Teams.GREEN_TEAM)

Analysis(team_memberships)
