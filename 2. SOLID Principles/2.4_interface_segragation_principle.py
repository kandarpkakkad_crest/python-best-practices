"""
Interface Segragation Principle.

This principle is the first principle that applies to Interfaces instead of classes in SOLID and
it is similar to the single responsibility principle. It states that “do not force any client to
implement an interface which is irrelevant to them“. Here your main goal is to focus on avoiding
fat interface and give preference to many small client-specific interfaces. You should prefer many
client interfaces rather than one general interface and each interface should have a specific
responsibility.

Suppose if you enter a restaurant and you are pure vegetarian. The waiter in that restaurant gave
you the menu card which includes vegetarian items, non-vegetarian items, drinks, and sweets. In
this case, as a customer, you should have a menu card which includes only vegetarian items, not
everything which you don’t eat in your food. Here the menu should be different for different types
of customers. The common or general menu card for everyone can be divided into multiple cards
instead of just one. Using this principle helps in reducing the side effects and frequency of
required changes.
"""


class IShapeOne:
    """Define a shape."""

    def draw_square(self):
        """Draw sqaure."""
        raise NotImplementedError

    def draw_rectangle(self):
        """Draw rectangle."""
        raise NotImplementedError

    def draw_circle(self):
        """Draw circle."""
        raise NotImplementedError


"""
This interface draws squares, circles, rectangles. class Circle, Square or
Rectangle implementing the IShape interface must define the methods
draw_square(), draw_rectangle(), draw_circle().
"""


class Circle(IShapeOne):
    """Define a circle."""

    def draw_square(self):
        """Draw square."""
        pass

    def draw_rectangle(self):
        """Draw rectangle."""
        pass

    def draw_circle(self):
        """Draw circle."""
        pass


class Square(IShapeOne):
    """Define a square."""

    def draw_square(self):
        """Draw square."""
        pass

    def draw_rectangle(self):
        """Draw rectangle."""
        pass

    def draw_circle(self):
        """Draw circle."""
        pass


class Rectangle(IShapeOne):
    """Define a rectangle."""

    def draw_square(self):
        """Draw square."""
        pass

    def draw_rectangle(self):
        """Draw rectangle."""
        pass

    def draw_circle(self):
        """Draw circle."""
        pass


"""
It’s quite funny looking at the code above. class Rectangle implements methods
(draw_circle and draw_square) it has no use of, likewise Square implementing
draw_circle, and draw_rectangle, and class Circle (draw_square, draw_rectangle).
If we add another method to the IShape interface, like draw_triangle(),
"""


class IShapeTwo:
    """Defina a shape."""

    def draw_square(self):
        """Draw sqaure."""
        raise NotImplementedError

    def draw_rectangle(self):
        """Draw rectangle."""
        raise NotImplementedError

    def draw_circle(self):
        """Draw circle."""
        raise NotImplementedError

    def draw_triangle(self):
        """Draw triangle."""
        raise NotImplementedError


"""
The classes must implement the new method or error will be thrown.
We see that it is impossible to implement a shape that can draw a circle but not
a rectangle or a square or a triangle.  We can just implement the methods to
throw an error that shows the operation cannot be performed.
ISP frowns against the design of this IShape interface. clients (here Rectangle,
Circle, and Square) should not be forced to depend on methods that they do not
need or use.  Also, ISP states that interfaces should perform only one job (just
like the SRP principle) any extra grouping of behavior should be abstracted away
to another interface.
Here, our IShape interface performs actions that should be handled independently
by other interfaces.
To make our IShape interface conform to the ISP principle, we segregate the
actions to different interfaces.  Classes (Circle, Rectangle, Square, Triangle,
etc) can just inherit from the IShape interface and implement their own draw
behavior.
"""


class IShape:
    """Define a shape."""

    def draw(self):
        """Draw shape."""
        raise NotImplementedError


class CircleOne(IShape):
    """Define a circle."""

    def draw(self):
        """Draw circle."""
        raise NotImplementedError


class SquareOne(IShape):
    """Define a square."""

    def draw(self):
        """Draw square."""
        raise NotImplementedError


class RectangleOne(IShape):
    """Define a rectangle."""

    def draw(self):
        """Draw rectangle."""
        raise NotImplementedError


"""
We can then use the I -interfaces to create Shape specifics like Semi Circle,
Right-Angled Triangle, Equilateral Triangle, Blunt-Edged Rectangle, etc.
"""
