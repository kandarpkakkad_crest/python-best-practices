"""
Liskov's Substitution Principle.

The principle was introduced by Barbara Liskov in 1987 and according to this principle “Derived or
child classes must be substitutable for their base or parent classes“. This principle ensures that
any class that is the child of a parent class should be usable in place of its parent without any
unexpected behavior.

You can understand it in a way that a farmer’s son should inherit farming skills from his father
and should be able to replace his father if needed. If the son wants to become a farmer then he
can replace his father but if he wants to become a cricketer then definitely the son can’t replace
his father even though they both belong to the same family hierarchy.

One of the classic examples of this principle is a rectangle having four sides. A rectangle’s
height can be any value and width can be any value. A square is a rectangle with equal width and
height. So we can say that we can extend the properties of the rectangle class into square class.
In order to do that you need to swap the child (square) class with parent (rectangle) class to fit
the definition of a square having four equal sides but a derived class does not affect the behavior
of the parent class so if you will do that it will violate the Liskov Substitution Principle.
Check the link Liskov Substitution Principle for better understanding.
"""


class AnimalOne:
    """Define an animal."""

    def __init__(self, name: str):
        r"""Initialize an animal.

        Args: \
            `name (str)`: Name of the animal.
        """
        self.name = name

    def get_name(self) -> str:
        r"""Get name.

        Returns: \
            `str`: Name of the animal.
        """
        self.name

    def make_sound(self):
        """Make sound."""
        pass


class Lion(AnimalOne):
    """Lion class."""

    def make_sound(self):
        """Make sound."""
        return "roar"


class Mouse(AnimalOne):
    """Mouse class."""

    def make_sound(self):
        """Make sound."""
        return "squeak"


class Pigeon(AnimalOne):
    """Snake class."""

    def make_sound(self):
        """Make sound."""
        return "something"


animals = [AnimalOne("lion"), AnimalOne("mouse"), AnimalOne("pigeon")]


def lion_leg_count() -> int:
    r"""Leg count.

    Returns: \
        `int`: 4
    """
    return 4


def mouse_leg_count() -> int:
    r"""Leg count.

    Returns: \
        `int`: 4
    """
    return 4


def pigeon_leg_count() -> int:
    r"""Leg count.

    Returns: \
        `int`: 2
    """
    return 2


def animal_leg_count(animals: list):
    r"""Leg count.

    Args: \
        `animals (list)`: Animals present.
    """
    for animal in animals:
        if isinstance(animal, Lion):
            print(lion_leg_count())
        elif isinstance(animal, Mouse):
            print(mouse_leg_count())
        elif isinstance(animal, Pigeon):
            print(pigeon_leg_count())


animal_leg_count(animals)

"""
To make this function follow the LSP principle, we will follow this LSP
requirements postulated by Steve Fenton:
If the super-class (Animal) has a method that accepts a super-class type
(Animal) parameter.  Its sub-class(Pigeon) should accept as argument a
super-class type (Animal type) or sub-class type(Pigeon type).  If the
super-class returns a super-class type (Animal).  Its sub-class should return a
super-class type (Animal type) or sub-class type(Pigeon).  Now, we can
re-implement animal_leg_count function:
"""


def animal_leg_count(animals: list):
    r"""Leg count.

    Args: \
        `animals (list)`: Animals present.
    """
    for animal in animals:
        print(animal.leg_count())


animal_leg_count(animals)

"""
The animal_leg_count function cares less the type of Animal passed, it just
calls the leg_count method.  All it knows is that the parameter must be of an
Animal type, either the Animal class or its sub-class.
The Animal class now have to implement/define a leg_count method.  And its
sub-classes have to implement the leg_count method:
"""


class Animal:
    """Define an animal."""

    def leg_count(self):
        """Leg count."""
        pass


class LionTwo(Animal):
    """Lion class."""

    def leg_count(self):
        """Leg count."""
        pass


"""
When it’s passed to the animal_leg_count function, it returns the number of legs
a lion has.
You see, the animal_leg_count doesn’t need to know the type of Animal to return
its leg count, it just calls the leg_count method of the Animal type because by
contract a sub-class of Animal class must implement the leg_count function.
"""
