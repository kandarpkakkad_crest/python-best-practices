"""
Single Responsibility Principle.

This principle states that “a class should have only one reason to change” which means every class
should have a single responsibility or single job or single purpose. Take the example of developing
software. The task is divided into different members doing different things as front-end designers
do design, the tester does testing and backend developer takes care of backend development part
then we can say that everyone has a single job or responsibility.

Most of the time it happens that when programmers have to add features or new behavior they
implement everything into the existing class which is completely wrong. It makes their code
lengthy, complex and consumes time when later something needs to be modified. Use layers in your
application and break God classes into smaller classes or modules.
"""


class Animal:
    """Define an animal."""

    def __init__(self, name: str) -> None:
        r"""Initialize animal.

        Args: \
            `name (str)`: Name of the animal.
        """
        self.name = name

    def get_name(self) -> str:
        r"""Get name of the animal.

        Returns: \
            `str`: Name of the animal.
        """
        return self.name

    def save(self) -> None:
        r"""Save animal.

        Args: \
            `animal (Animal)`: Animal object.
        """


"""
The Animal class violates the SRP.

How does it violate SRP?
SRP states that classes should have one responsibility, here, we can draw out two responsibilities:
    1. animal database management
    2. animal properties management.
The constructor and get_name manage the Animal properties while the save manages the Animal storage
on a database.

How will this design cause issues in the future?
If the application changes in a way that it affects database management functions. The classes that
make use of Animal properties will have to be touched and recompiled to compensate for the new
changes.

You see this system smells of rigidity, it’s like a domino effect, touch one card it affects all
other cards in line.

To make this conform to SRP, we create another class that will handle the sole responsibility of
storing an animal to a database:
"""


class AnimalOne:
    """Define an animal."""

    def __init__(self, name: str) -> None:
        r"""Initialize animal.

        Args: \
            `name (str)`: Name of the animal.
        """
        self.name = name

    def get_name(self) -> str:
        r"""Get name of the animal.

        Returns: \
            `str`: Name of the animal.
        """
        return self.name


class AnimalDB:
    """Animal Database."""

    def get_animal(self) -> AnimalOne:
        r"""Get the animal.

        Returns: \
            `AnimalOne`: the animal.
        """

    def save(self, animal: AnimalOne) -> None:
        r"""Save animal.

        Args: \
            `animal (Animal)`: Animal object.
        """


"""
When designing our classes, we should aim to put related features together,so whenever they tend
to change they change for the same reason. And we should try to separate features if they will
change for different reasons. - Steve Fenton
"""
