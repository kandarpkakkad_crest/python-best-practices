"""
How to choose names.

>>> Case 1:

>>> # Not recommended
>>> x = 'John Smith'
>>> y, z = x.split()
>>> print(z, y, sep=', ')
'Smith, John'

>>> # Recommended
>>> name = 'John Smith'
>>> first_name, last_name = name.split()
>>> print(last_name, first_name, sep=', ')
'Smith, John'

>>> Case 2:

>>> # Not recommended
>>> def db(x):
>>>     return x * 2

>>> # Recommended
>>> def multiply_by_two(x):
>>>     return x * 2
"""
