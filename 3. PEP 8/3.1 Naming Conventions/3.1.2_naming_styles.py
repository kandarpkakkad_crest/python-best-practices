r"""
Naming Styles.

Functions: \
    Use a lowercase word or words. Separate words by underscores to improve readability. \
    >>> function, my_function

Classes: \
    Start each word with a capital letter. Do not separate words with underscores.
    This style is called camel case. \
    >>> Model, MyClass

Variables: \
    Use a lowercase single letter, word, or words. Separate words with underscores to
    improve readability. \
    >>> x, var, my_variable

Methods: \
    Use a lowercase word or words. Separate words with underscores to improve readability. \
    >>> class_method, method

Constants: \
    Use an uppercase single letter, word, or words. Separate words with underscores to
    improve readability. \
    >>> CONSTANT, MY_CONSTANT, MY_LONG_CONSTANT

Modules: \
    Use a short, lowercase word or words. Separate words with underscores to improve
    readability. \
    >>> module.py, my_module.py

Packages: \
    Use a short, lowercase word or words. Do not separate words with underscores. \
    >>> package, mypackage
"""
