"""
Whitespace Arounf Binary Operators.

Surround the following binary operators with a single space on either side:
    * Assignment operators (=, +=, -=, and so forth)
    * Comparisons (==, !=, >, <. >=, <=) and (is, is not, in, not in)
    * Booleans (and, not, or)

NOTE: When = is used to assign a default value to a function argument, do not surround it
with spaces.

>>> # Recommended
>>> def function(default_parameter=5):
>>>     # ...

>>> # Not recommended
>>> def function(default_parameter = 5):
>>>     # ...

When there’s more than one operator in a statement, adding a single space before and after
each operator can look confusing. Instead, it is better to only add whitespace around the
operators with the lowest priority, especially when performing mathematical manipulation.
Here are a couple examples:

>>> # Recommended
>>> y = x**2 + 5
>>> z = (x+y) * (x-y)

>>> # Not Recommended
>>> y = x ** 2 + 5
>>> z = (x + y) * (x - y)

>>> # Not recommended
>>> if x > 5 and x % 2 == 0:
>>>     print('x is larger than 5 and divisible by 2!')

>>> # Recommended
>>> if x>5 and x%2==0:
>>>     print('x is larger than 5 and divisible by 2!')

>>> # Definitely do not do this!
>>> if x >5 and x% 2== 0:
>>>     print('x is larger than 5 and divisible by 2!')

In slices, colons act as a binary operators. Therefore, the rules outlined in the previous
section apply, and there should be the same amount of whitespace either side. The following
examples of list slices are valid:

>>> list[3:4]
>>>
>>> # Treat the colon as the operator with lowest priority
>>> list[x+1 : x+2]
>>>
>>> # In an extended slice, both colons must be
>>> # surrounded by the same amount of whitespace
>>> list[3:4:5]
>>> list[x+1 : x+2 : x+3]
>>>
>>> # The space is omitted if a slice parameter is omitted
>>> list[x+1 : x+2 :]
"""
