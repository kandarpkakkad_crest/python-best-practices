"""
When To Avoid Adding Whitespaces.

In some cases, adding whitespace can make code harder to read. Too much whitespace can make
code overly sparse and difficult to follow. PEP 8 outlines very clear examples where
whitespace is inappropriate.

The most important place to avoid adding whitespace is at the end of a line. This is known as
trailing whitespace. It is invisible and can produce errors that are difficult to trace.

The following list outlines some cases where you should avoid adding whitespace:

    1. Immediately inside parentheses, brackets, or braces:

    >>> # Recommended
    >>> my_list = [1, 2, 3]
    >>>
    >>> # Not recommended
    >>> my_list = [ 1, 2, 3, ]

    2. Before a comma, semicolon, or colon:

    >>> x = 5
    >>> y = 6
    >>>
    >>> # Recommended
    >>> print(x, y)
    >>>
    >>> # Not recommended
    >>> print(x , y)

    3. Before the open parenthesis that starts the argument list of a function call:

    >>> def double(x):
    >>>     return x * 2
    >>>
    >>> # Recommended
    >>> double(3)
    >>>
    >>> # Not recommended
    >>> double (3)

    4. Before the open bracket that starts an index or slice:

    >>> # Recommended
    >>> list[3]
    >>>
    >>> # Not recommended
    >>> list [3]

    5. Between a trailing comma and a closing parenthesis:

    >>> # Recommended
    >>> tuple = (1,)
    >>>
    >>> # Not recommended
    >>> tuple = (1, )

    6. To align assignment operators:

    >>> # Recommended
    >>> var1 = 5
    >>> var2 = 6
    >>> some_long_var = 7
    >>>
    >>> # Not recommended
    >>> var1          = 5
    >>> var2          = 6
    >>> some_long_var = 7
"""
