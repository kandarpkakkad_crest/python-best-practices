"""
Where To Put The Closing Brace.

PEP 8 provides two options for the position of the closing brace in implied line
continuations:

    1. Line up the closing brace with the first non-whitespace character of the previous
    line:

    >>> list_of_numbers = [
    >>>     1, 2, 3,
    >>>     4, 5, 6,
    >>>     7, 8, 9
    >>>     ]

    2. Line up the closing brace with the first character of the line that starts the
    construct:

    >>> list_of_numbers = [
    >>>     1, 2, 3,
    >>>     4, 5, 6,
    >>>     7, 8, 9
    >>> ]
"""
