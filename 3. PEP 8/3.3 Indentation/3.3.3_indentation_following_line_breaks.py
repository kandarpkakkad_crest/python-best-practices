"""
Indentation Following Line Breaks.

When you’re using line continuations to keep lines to under 79 characters, it is useful to
use indentation to improve readability. It allows the reader to distinguish between two lines
of code and a single line of code that spans two lines. There are two styles of indentation
you can use.

The first of these is to align the indented block with the opening delimiter:

>>> def function(arg_one, arg_two,
>>>             arg_three, arg_four):
>>>    return arg_one

Sometimes you can find that only 4 spaces are needed to align with the opening delimiter.
This will often occur in if statements that span multiple lines as the if, space, and opening
bracket make up 4 characters. In this case, it can be difficult to determine where the nested
code block inside the if statement begins:

>>> x = 5
>>> if (x > 3 and
>>>     x < 10):
>>>     print(x)

In this case, PEP 8 provides two alternatives to help improve readability:

    1. Add a comment after the final condition. Due to syntax highlighting in most editors, this will
    separate the conditions from the nested code:

    >>> x = 5
    >>> if (x > 3 and
    >>>     x < 10):
    >>>     # Both conditions satisfied
    >>>     print(x)

    2. Add extra indentation on the line continuation:

    >>> x = 5
    >>> if (x > 3 and
    >>>         x < 10):
    >>>     # Both conditions satisfied
    >>>     print(x)

An alternative style of indentation following a line break is a hanging indent. This is a
typographical term meaning that every line but the first in a paragraph or statement is
indented. You can use a hanging indent to visually represent a continuation of a line of code.
Here’s an example:

>>> var = function(
>>>     arg_one, arg_two,
>>>     arg_three, arg_four)

NOTE: When you’re using a hanging indent, there must not be any arguments on the first line.
The following example is not PEP 8 compliant:

>>> # Not Recommended
>>> var = function(arg_one, arg_two,
>>>     arg_three, arg_four)

When using a hanging indent, add extra indentation to distinguish the continued line from
code contained inside the function. The following example is difficult to read because the
code inside the function is at the same indentation level as the continued lines:

>>> # Not Recommended
>>> def function(
>>>     arg_one, arg_two,
>>>     arg_three, arg_four):
>>>     return arg_one

Instead, it’s better to use a double indent on the line continuation. This helps you to
distinguish between function arguments and the function body, improving readability:

>>> def function(
>>>         arg_one, arg_two,
>>>         arg_three, arg_four):
>>>     return arg_one
"""
