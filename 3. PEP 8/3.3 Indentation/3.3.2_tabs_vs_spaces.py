"""
Tabs vs Spaces.

If you’re using Python 2 and have used a mixture of tabs and spaces to indent your code,
you won’t see errors when trying to run it. To help you to check consistency, you can add a
-t flag when running Python 2 code from the command line. The interpreter will issue warnings
when you are inconsistent with your use of tabs and spaces:

>>> $ python2 -t code.py
>>> code.py: inconsistent use of tabs and spaces in indentation

If, instead, you use the -tt flag, the interpreter will issue errors instead of warnings,
and your code will not run. The benefit of using this method is that the interpreter tells
you where the inconsistencies are:

>>> $ python2 -tt code.py
>>>   File "code.py", line 3
>>>     print(i, j)
>>>              ^
>>> TabError: inconsistent use of tabs and spaces in indentation

Python 3 does not allow mixing of tabs and spaces. Therefore, if you are using Python 3,
then these errors are issued automatically:

>>> $ python3 code.py
>>>   File "code.py", line 3
>>>     print(i, j)
>>>               ^
>>> TabError: inconsistent use of tabs and spaces in indentation
"""
