r"""
Maximum Line Length And Line Breaking.

PEP 8 suggests lines should be limited to 79 characters. This is because it allows you to
have multiple files open next to one another, while also avoiding line wrapping. But we here
at Crest follow 120 character line limit.

>>> def function(arg_one, arg_two,
>>>              arg_three, arg_four):
>>>     return arg_one

>>> from mypkg import example1, \
>>>     example2, example3

>>> # Recommended
>>> total = (first_variable
>>>          + second_variable
>>>          - third_variable)

>>> # Not Recommended
>>> total = (first_variable +
>>>          second_variable -
>>>          third_variable)
"""
