"""
File Summary.

Reference: https://realpython.com/documenting-python-code/
"""

import math


def function_1(param_1: str, param_2: int = 13) -> str:
    r"""Short summary.

    Extended summary.

    Args: \
        `param_1 (str)`: Description. \
        `param_2 (int, optional)`: Description. Defaults to 13.

    Returns: \
        `str`: Description.
    """
    # Input
    input_1 = input("Take input: ")

    # Calculation
    input_1 = input_1 * param_2 * math.sqrt(3)
    param_1 += str(input_1)

    # Output
    print("Input 1: {}\nParam 1: {}".format(input_1, param_1))

    return param_1


class ClassOne:
    """Class summary."""

    def __init__(self, parameter_1: int) -> None:
        r"""Initialize ClassOne.

        Args: \
            `parameter_1 (int)`: Description.
        """
        # Initialize group-1 parameters.
        self.parameter_1 = parameter_1

    def class_1_function_1(self, parameter_1: int) -> str:
        """Short summary.

        Extended summary.

        Args:
            parameter_1 (int): Description.

        Returns:
            str: Description.
        """
        # Calculation
        parameter_1 *= 15

        return str(parameter_1)
